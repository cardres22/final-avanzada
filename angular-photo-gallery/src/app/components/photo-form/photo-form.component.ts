import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {PhotoService} from '../../services/photo.service';
import {Router} from '@angular/router';


interface htmlInputEvent extends Event {
  target: HTMLInputElement & EventTarget;

}
@Component({
  selector: 'app-photo-form',
  templateUrl: './photo-form.component.html',
  styleUrls: ['./photo-form.component.css']
})
export class PhotoFormComponent implements OnInit {

  file:File;
  fotoSelected:string | ArrayBuffer;
  constructor(private photoService:PhotoService, private router:Router) { }

  ngOnInit(): void {
  }
  selecionar_foto(event: htmlInputEvent): void{
    if(event.target.files && event.target.files[0]){
      this.file=<File>event.target.files[0];
      const reader = new FileReader();
      reader.onload = e => this.fotoSelected = reader.result;
      reader.readAsDataURL(this.file);
    }
  }
  cargar_foto(titulo:HTMLInputElement, desc:HTMLTextAreaElement): Boolean {
    this.photoService.createPhoto(titulo.value,desc.value,this.file)
    .subscribe(res => this.router.navigate(['/photos']), err=>console.log(err));
    console.log(titulo.value);
    console.log(desc.value);
    return false;
  }
}
